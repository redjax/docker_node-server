"use strict";

// Imports
const express = require("express");
// const { get } = require('http')
// const path = require('path')
const favicon = require("serve-favicon");
const nunjucks = require("nunjucks")
const bodyParser = require("body-parser")

// My modules
const logs = require("./modules/logger");
// Assign logger module's "logger" function to constant "log"
const log = logs.logger;
// Assign logger module's "httpLoger" function to constant "hlog"
const hlog = logs.httpLogger;

// Import config file in ./config/app_config.js
var config_file = require("./config/app_config");
// Config file has a confusing nested object modle. Create a variable to "flatten" the first level
var config = config_file.config;
// Shortcut to config_file.config.sitevars
const sitevars = config.sitevars;
const siteconfig = config.site_config;
// const getNestedObject = config_file.getNestedObject
const secrets = config.secrets;

log.debug(`App config print: ${JSON.stringify(config)}`);
log.debug(`App sitevars print: ${JSON.stringify(sitevars)}`);
log.debug(`App print: Secrets: ${secrets.secret1}`);

// METHOD 1: the getNestedObject function. Uncomment "const getNestedObject" above
//  if using this method
// var favicon_path = getNestedObject(config, ['site_config', 'favicon_path'])
//
// METHOD 2: access the favicon_path config variable directly.
//  Comment getNestedObject if using this method
var favicon_path = siteconfig.favicon_path;

log.debug(`App debug: favicon path: ${favicon_path}`);

// Create express app, port, and host
var app = express();
// Tell app to use logger.httpLogger middleware
app.use(hlog);

// Configure nunjucks templating
nunjucks.configure('views', {
  autoescape: true,
  express: app
})

// const port = 8080
const port = process.env.NODE_PORT;

const host = "0.0.0.0";

// set view engine
const view_engine = "html";
app.set("view engine", view_engine);

// Set static directory to './public'
const static_dir = express.static("public");
app.use(static_dir);
// Set favicon
app.use(favicon(favicon_path));

// Tell app to use error logging and handling functions
app.use(logErrors);
app.use(errorHandler);

// Add body parser for i.e. webforms
app.use(express.urlencoded({ extended: true }));
// Parse json bodies sent by API clients
app.unsubscribe(express.json());

// Log NODE_ENV to console if NODE_ENV is not 'production'

// App
const router = express.Router();

// Routes

// index page
router.get("/", (req, res, next) => {
  // Example, pass an array to the page. Check ejs for syntax on looping elements
  var mascots = [
    { name: "Sammy", organization: "Digital Ocean", birth_year: 2012 },
    { name: "Tux", organization: "Linux", birth_year: 1996 },
    { name: "Moby Dock", organization: "Docker", birth_year: 2013 },
  ];

  // When page loads, renders a paragraph with <%= tagline %>
  var tagline =
    "No programming concept is complete without a cute animal mascot.";

  log.debug(mascots);

  // Render pages/index.ejs
  res.status(200).render("live/pages/index.html", {
    // pass sitevars to all pages
    sitevars,
    // Assign variables above to new variable, pass to template
    mascots: mascots,
    tagline: tagline,
    sitevars,
    active: "home",
  });
});

// about page
router.get("/about", (req, res) => {
  var example_img = "/img/stream-stones-trees.jpg";
  var card_img = "/img/spooky-ocean.jpg"

  log.debug(`img: ${example_img}`);

  res.render("live/pages/about.html", {
    // pass sitevars to all pages
    sitevars,
    example_img,
    active: "about",
    card_img: card_img
  });
});

// Example page, demonstrating page components/partials and Bootstrap components
router.get("/examples", (req, res, next) => {

  var figure_img = "/img/ladybug.jpg";

  res.render("live/pages/examples.html", {
    sitevars,
    active: "examples",
    figure_img: figure_img
  });
});

router.post("/examples/submit-form", (req, res, next) => {

  log.debug(`POST body: ${req.body}`)
  var text1 = req.body['text1'],
    text2 = req.body['text2']

  log.debug(`Form text1 data: ${text1}`)
  log.debug(`Form text2 data: ${text2}`)

    res.writeHead(302, {
      "location": "/examples"
    })
  res.end()
})

router.get("/test", (req, res, next) => {
  res.render("live/pages/test.html", {
    sitevars,
    active: "test"
  });
});

// Example error logging
router.get("/boom", (req, res, next) => {
  try {
    throw new Error("Wowza!");
  } catch (error) {
    log.error(`Whoops! This broke with error: ${error}`);
    res.status(500).send("Error!");
  }
});

router.get("/errorhandler", (req, res, next) => {
  try {
    throw new Error("Wowza!");
  } catch (error) {
    next(error);
  }
});

function logErrors(err, req, res, next) {
  // Log errors to console
  console.error(err.stack);
  next(err);
}

function errorHandler(err, req, res, next) {
  // When page cannot resolve, send error message
  res.status(500).send("Error 500");
}

// Only print debug info to console if in development environment
if (process.env.NODE_ENV !== "production") {
  log.info(`NODE_ENV: ${process.env.NODE_ENV}`);
  log.debug(`.env host: ${host}`);
  log.debug(`.env port: ${port}`);
  log.debug(`view engine: ${view_engine}`);
  log.debug(`sitevars: ${JSON.stringify(sitevars)}`);
  log.debug(`Log file: ${logs.opts.file.filename}`);

  // Print newline on dev to separate logs
  console.log(`\n`);
}

// Setup router
app.use("/", router);

// Run server
app.listen(process.env.NODE_PORT, host || 8080, host);
log.info(`Server running in Docker on http://${host}:${port}`);
